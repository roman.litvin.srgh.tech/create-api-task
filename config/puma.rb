# frozen_string_literal: true

rackup      DefaultRackup
environment ENV['RACK_ENV'] || 'development'
