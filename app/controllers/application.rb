# frozen_string_literal: true

module Controllers
  class Application < Base
    use Api::Users
    use Api::Posts
    use Api::Ratings
  end
end
