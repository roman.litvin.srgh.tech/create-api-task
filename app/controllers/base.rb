# frozen_string_literal: true

module Controllers
  class Base < Sinatra::Base
    use Rack::JSONBodyParser
    register Sinatra::Namespace
    register Sinatra::ActiveRecordExtension

    set :show_exceptions, :after_handler

    configure do
      set :server, :puma
    end

    configure :development do
      require 'sinatra/reloader'
      register Sinatra::Reloader
    end
  end
end
