# frozen_string_literal: true

module Controllers
  module Api
    class Posts < Base
      namespace '/api/v0/posts' do
        post '/new' do
          ActiveRecord::Base.transaction do
            author = User.find_or_create_by!(login: post_params[:login])
            author.posts.create!(post_params.slice(:title, :body, :author_ip))
          end

          status 200
          json(status: 200, message: post_params)
        rescue ActiveRecord::RecordInvalid => e
          status 400
          json(status: 400, message: e)
        end

        get '/rating' do
          number = params[:top].to_i || 10
          status 200
          json Post.top_list(number)
        rescue ActiveRecord::RecordInvalid => e
          status 400
          json(status: 1, message: e)
        end

        get '/list/ip' do
          status 200
          json Post.list_ip
        end

        private

        def post_params
          params.slice(:login, :title, :body, :user_ip)
        end
      end
    end
  end
end
