# frozen_string_literal: true

module Controllers
  module Api
    class Ratings < Base
      namespace '/api/v0' do
        post '/ratings/:post_id' do |post_id|
          post = Post.find_by(id: post_id)
          return json(status: 1, message: 'Post must exist') if post.blank?

          user = User.find_by(login: rating_params[:login])
          return json(status: 1, message: 'Post must exist') if user.blank?

          Rating.create!(user: user, post: post, value: rating_params[:value])

          status 200
          json(status: 0, message: { post_average_rating: post.calculate_average_rating })
        rescue ActiveRecord::RecordInvalid => e
          status 400
          json(status: 1, message: e)
        end

        private

        def rating_params
          params.slice(:login, :value)
        end
      end
    end
  end
end
