# frozen_string_literal: true

module Controllers
  module Api
    class Users < Base
      namespace '/api/v0' do
        post '/users/new' do
          user = User.new(user_params)
          if user.save
            status 200
            json(status: 200, message: 'ok')
          else
            status 400
            json(status: 400, message: user.errors.messages)
          end
        end

        private

        def user_params
          params.slice(:login)
        end
      end
    end
  end
end
