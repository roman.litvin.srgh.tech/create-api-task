# frozen_string_literal: true

class User < ApplicationRecord
  has_many :posts, class_name: 'Post', foreign_key: :user_id

  validates :login, presence: true, uniqueness: true
end
