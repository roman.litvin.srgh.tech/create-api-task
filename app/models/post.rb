# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :author, class_name: 'User', foreign_key: :user_id
  has_many :ratings

  validates :body, presence: true
  validates :title, presence: true

  def calculate_average_rating
    rating_post = ratings.map(&:value)
    (rating_post.inject { |value, el| value + el }.to_f / rating_post.size).round(2)
  end

  def self.top_list(number)
    Rating.select(
      [
        Arel::Nodes::NamedFunction.new(
          'ROUND', [
            Arel::Nodes::NamedFunction.new('SUM', [Rating.arel_table[:value]]) / Rating.arel_table[:post_id].count, 2
          ]
        ).as('avg_rate'), Post.arel_table[:id], Post.arel_table[:title], Post.arel_table[:body]
      ]
    ).joins(
      Rating.arel_table.join(Post.arel_table).on(
        Rating.arel_table[:post_id].eq(Post.arel_table[:id])
      ).join_sources
    ).order(:avg_rate).reverse_order.group(
      Post.arel_table[:id], Post.arel_table[:title], Post.arel_table[:body]
    ).limit(number)
  end

  def self.list_ip
    self.select(
      [Post.arel_table[:author_ip].as('ip'),
       Arel::Nodes::NamedFunction.new('ARRAY_AGG', [User.arel_table[:login]]).as('logins')]
    ).joins(:author).group(Post.arel_table[:author_ip]).as_json(except: :id)
  end
end
