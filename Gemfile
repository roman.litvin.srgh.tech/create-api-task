# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.4'

gem 'activerecord', '~> 6'
gem 'activesupport', '~> 6'
gem 'faraday'
gem 'faraday_middleware'
gem 'pg'
gem 'puma'
gem 'rack'
gem 'rack-contrib', require: 'rack/contrib'
gem 'rake'
gem 'sinatra', require: 'sinatra/base'
gem 'sinatra-activerecord', require: 'sinatra/activerecord'
gem 'sinatra-contrib'
gem 'zeitwerk'

group :test do
  gem 'database_cleaner-active_record'
  gem 'rack-test'
  gem 'rspec'
  gem 'shoulda-matchers'
  gem 'simplecov'
end

group :development, :test do
  gem 'byebug'
  gem 'factory_bot'
  gem 'faker'
end

group :development do
  gem 'rubocop', require: false
end
