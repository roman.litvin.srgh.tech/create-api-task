class CreateRatingsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
      t.references :user
      t.references :post
      t.integer :value

      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
