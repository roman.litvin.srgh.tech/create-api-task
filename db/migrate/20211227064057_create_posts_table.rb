class CreatePostsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.references :user, foreign_key: { to_table: :users }
      t.inet :author_ip
      t.string :title
      t.text :body

      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
