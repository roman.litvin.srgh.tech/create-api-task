# frozen_string_literal: true

ips = Array.new(50) { Faker::Internet.unique.public_ip_v4_address }
construct_request = Faraday.new(url: 'http://localhost:9292') do |faraday|
  faraday.request :url_encoded
  faraday.response :logger, nil, headers: false, bodies: true
  faraday.response :json
end
100.times do
  req_url = 'api/v0/users/new'
  data = {
    login: Faker::Internet.unique.username
  }
  construct_request.post(req_url, data)
end

200_000.times do
  req_url = 'api/v0/posts/new'
  data = {
    login: User.all.sample.login,
    author_ip: ips.sample,
    title: Faker::Lorem.sentence,
    body: Faker::Lorem.paragraph
  }
  construct_request.post(req_url, data)
end

rand(50_000..150_000).times do
  post_id = Post.all.sample.id
  req_url = "api/v0/ratings/#{post_id}"
  data = {
    login: User.all.sample.login,
    value: rand(1..5)
  }
  construct_request.post(req_url, data)
end
