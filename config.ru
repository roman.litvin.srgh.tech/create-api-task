# frozen_string_literal: true

require_relative './dependencies'

map '/' do
  run Controllers::Application
end
