# frozen_string_literal: true

require 'bundler'

RACK_ENV ||= (ENV['RACK_ENV'] || :development)
Bundler.require(:default, RACK_ENV.to_sym)

APP_LOADER = Zeitwerk::Loader.new
%w[
  app
  app/models
].each(&APP_LOADER.method(:push_dir))
APP_LOADER.setup
APP_LOADER.eager_load
