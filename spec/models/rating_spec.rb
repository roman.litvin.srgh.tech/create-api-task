# frozen_string_literal: true

describe Rating, type: :model do
  let(:rating) { described_class.new }

  describe 'DB Columns' do
    it { is_expected.to have_db_column(:value).of_type(:integer) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:post_id).of_type(:integer) }
  end

  describe 'Validations' do
    it { expect(rating).to validate_presence_of :value }
    it 'ok' do
      expect(rating).to validate_uniqueness_of(:user_id).scoped_to(:post_id)
    end
    it 'ok' do
      expect(rating)
        .to validate_numericality_of(:value)
        .is_greater_than_or_equal_to(1)
        .is_less_than_or_equal_to(5)
    end
  end

  describe 'Associations' do
    it { is_expected.to belong_to(:post) }
    it { is_expected.to belong_to(:user) }
  end
end
