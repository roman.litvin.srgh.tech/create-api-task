# frozen_string_literal: true

describe Post, type: :model do
  let(:post) { described_class.new }

  describe 'DB Columns' do
    it { is_expected.to have_db_column(:author_ip).of_type(:inet) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:title).of_type(:string) }
    it { is_expected.to have_db_column(:body).of_type(:text) }
  end

  describe 'Validations' do
    it { expect(post).to validate_presence_of :title }
    it { expect(post).to validate_presence_of :body }
  end

  describe 'Associations' do
    it { is_expected.to belong_to(:author) }
    it { is_expected.to have_many(:ratings) }
  end
end
