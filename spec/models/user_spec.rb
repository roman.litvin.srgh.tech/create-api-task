# frozen_string_literal: true

describe User, type: :model do
  let(:user) { described_class.new }

  describe 'DB Columns' do
    it { is_expected.to have_db_column(:login).of_type(:string) }
  end

  describe 'Validations' do
    it { expect(user).to validate_presence_of :login }
  end

  describe 'Associations' do
    it { is_expected.to have_many(:posts) }
  end
end
