# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    author factory: :user

    title { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }
    author_ip { Faker::Internet.public_ip_v4_address }
  end
end
