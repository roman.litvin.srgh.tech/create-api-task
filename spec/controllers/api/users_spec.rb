# frozen_string_literal: true

require 'rack/test'

describe Controllers::Api::Users do
  include Rack::Test::Methods
  FactoryBot.find_definitions

  let(:route) { '/api/v0/users/new' }
  let(:login) { Faker::Internet.unique.username }
  let(:body) { JSON.parse(last_response.body) }

  describe 'POST /api/v0/users/new' do
    context 'Valid attributes' do
      let(:attributes) do
        {
          login: login
        }
      end

      before do
        post(route, attributes)
      end
      it { expect(last_response).to be_successful }

      it { expect(last_response.status).to eq 200 }
      it { expect(body).to match('status' => 200, 'message' => 'ok') }
    end

    context 'when no login' do
      let(:attributes) do
        {
          login: ''
        }
      end

      before do
        post(route, attributes)
      end

      it { expect(last_response).not_to be_successful }
      it { expect(last_response.status).to eq 400 }
      it do
        expect(body).to match('status' => 400, 'message' => { 'login' => ["can't be blank"] })
      end
    end
  end
end
