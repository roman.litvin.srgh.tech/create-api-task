# frozen_string_literal: true

require 'rack/test'

describe Controllers::Api::Ratings do
  include Rack::Test::Methods
  FactoryBot.find_definitions

  let(:user) { FactoryBot.create(:user) }
  let(:user_post) { FactoryBot.create(:post, author: user, author_ip: '192.168.1.1') }
  let(:route) { "/api/v0/ratings/#{user_post.id}" }
  let(:body) { JSON.parse(last_response.body) }

  describe 'POST /api/v0/ratings/:post_id' do
    context 'Valid attributes' do
      let(:attributes) do
        {
          login: user.login,
          value: 1
        }
      end

      before do
        post(route, attributes)
      end
      it { expect(last_response).to be_successful }
      it { expect(last_response.status).to eq 200 }
      it do
        expect(body).to match('status' => 0, 'message' => { 'post_average_rating' => 1.0 })
      end
    end

    context 'when wrong value' do
      let(:attributes) do
        {
          login: user.login,
          value: 0
        }
      end

      before do
        post(route, attributes)
      end

      it { expect(last_response).not_to be_successful }
      it { expect(last_response.status).to eq 400 }
      it do
        expect(body).to match('status' => 1, 'message' => 'Validation failed: Value must be greater than or equal to 1')
      end
    end
  end
end
