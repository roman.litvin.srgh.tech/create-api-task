# frozen_string_literal: true

require 'rack/test'

describe Controllers::Api::Posts do
  include Rack::Test::Methods
  FactoryBot.find_definitions

  let(:route) { '/api/v0/posts/new' }
  let(:author) { FactoryBot.create(:user) }
  let(:body) { JSON.parse(last_response.body) }

  describe 'POST /api/v0/posts/new' do
    context 'Valid attributes' do
      let(:attributes) do
        {
          login: author.login,
          title: 'some title',
          body: 'some body',
          user_ip: '192.168.1.1'
        }
      end

      before do
        post(route, attributes)
      end
      it { expect(last_response).to be_successful }

      it { expect(last_response.status).to eq 200 }
      it do
        expect(body).to match(
          'status' => 200,
          'message' => {
            'login' => author.login,
            'title' => 'some title',
            'body' => 'some body',
            'user_ip' => '192.168.1.1'
          }
        )
      end
    end

    context 'when no login' do
      let(:attributes) do
        {
          login: '',
          title: 'some title',
          body: 'some body',
          author_ip: '192.168.1.1'
        }
      end

      before do
        post(route, attributes)
      end

      it { expect(last_response).not_to be_successful }
      it { expect(last_response.status).to eq 400 }
      it do
        expect(body).to match('status' => 400, 'message' => "Validation failed: Login can't be blank")
      end
    end

    context 'when no title' do
      let(:attributes) do
        {
          login: author.login,
          title: '',
          body: 'some body',
          author_ip: '192.168.1.1'
        }
      end

      before do
        post(route, attributes)
      end

      it { expect(last_response).not_to be_successful }
      it { expect(last_response.status).to eq 400 }
      it do
        expect(body).to match('status' => 400, 'message' => "Validation failed: Title can't be blank")
      end
    end

    context 'when no body' do
      let(:attributes) do
        {
          login: author.login,
          title: 'some title',
          body: '',
          author_ip: '192.168.1.1'
        }
      end

      before do
        post(route, attributes)
      end

      it { expect(last_response).not_to be_successful }
      it { expect(last_response.status).to eq 400 }
      it do
        expect(body).to match('status' => 400, 'message' => "Validation failed: Body can't be blank")
      end
    end
  end
end
