# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
require_relative '../dependencies'
require 'rspec'
require 'rack/test'
require 'shoulda/matchers'
require 'database_cleaner-active_record'
require 'simplecov'

SimpleCov.start

module RSpecMixin
  include Rack::Test::Methods
  def app
    Sinatra::Application
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec

    with.library :active_record
    with.library :active_model
  end

  def app
    Controllers::Application
  end
end

RSpec.configure do |config|
  config.include Rack::Test::Methods

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
